
# NA18 – Groupe 8 – NDC
# Plateforme d'échange et de revente de jeux vidéos et consoles
##### Arthur Jouglet – Thomas Dedinsky - Adrien Charannat

## Objet > Annonce 
- Numéro {clé}
- Titre
- Description
- Type
- Date d’ajout
- Prix
- Jeu(x) vidéo(s) / Console(s) de jeu(x)
- Annonceur

## Objet > Utilisateur
- Identifiant {clé}
- Nom
- Prénom
- Pseudonyme
- Solde (porte-monnaie en ligne)
- Date d'inscription
- Jeu vidéo favori
- Adresse e-mail

## Objet > Utilisateur > Administrateur
##### Hérite de la classe "Utilisateur"

## Objet > Jeu Vidéo
- Nom
- Date de sortie
- Plateforme
- Note (moyenne des "Evaluation")

## Objet > Console de Jeu

- Nom
- Modèle
- Date de sortie

## Objet > Evaluation
- Note
- Commentaire
- Date
- Jeu vidéo / Console
- Utilisateur

## Objet > Message
- Émetteur
- Récepteur
- Objet
- Contenu
- Date

## Objet > Transaction
- Date
- Méthode de paiement {CB, PayPal, solde}
- Montant
- Annonce
- Débiteur
- Créditeur

## Objet > Echange
- Date
- Participants

## Utilisateurs et Administrateurs
-   Utilisateur : crée et gère ses propres annonces de vente, d'achat ou de vente de jeux et consoles ; peut acheter des articles depuis les annonces d'autres utilisateurs ; peut publier des évaluations sur un jeu ou une console
-   Administrateur : modère les utilisateurs et leurs publications

-   Les utilisateurs peuvent ajouter de nouveaux jeux vidéo et développeurs à la BDD
-   Les utilisateurs peuvent refuser une transaction en attente jusqu'à ce que les deux membres concernés la confirment
-   Les membres peuvent remplir leur solde par la transaction d’une de leurs annonces
-   Les administrateurs peuvent invalider les transactions des utilisateurs
