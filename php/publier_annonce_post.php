<html>
<link rel="stylesheet" href="themes.css">
<head><meta charset="UTF-8"></head>
<body>
    <h1>Publication Annonce</h1>
<p>
<?php
  $connexion = new PDO('pgsql:host=tuxa.sme.utc;dbname=dbbdd0a040','bdd0a040','r4HeOFDb');

  // Could have the primary key auto-incriment using SQL instead
  $sql = 'INSERT INTO ANNONCE VALUES ((SELECT MAX(Num_Annonce) + 1 FROM ANNONCE), :titre, :type, :lieu, :date_annonce, :description, :prix, :mail_utilisateur, :code_produit);';
  $result = $connexion->prepare($sql);
  
  $result->bindValue(':titre', $_GET["Titre"], PDO::PARAM_STR);
  $result->bindValue(':type', $_GET["Type"], PDO::PARAM_STR);
  $result->bindValue(':lieu', $_GET["Lieu"], PDO::PARAM_STR);
  date_default_timezone_set('Europe/Paris');
  $result->bindValue(':date_annonce', date("Y-m-d"), PDO::PARAM_STR);
  $result->bindValue(':description', $_GET["Description"], PDO::PARAM_STR);
  $result->bindValue(':prix', $_GET["Prix"], PDO::PARAM_STR);
  $result->bindValue(':mail_utilisateur', $_GET["Mail_Utilisateur"], PDO::PARAM_STR);
  $result->bindValue(':code_produit', $_GET["Code_Produit"], PDO::PARAM_INT);

  $result->execute();

  if ($result) {
    echo 'Nouveau inséré';
  }
  else {
    echo 'Erreur lors de l\'insertion';
  }

?>
</p>
</body>
</html>