<html>
  <head>
    <link rel="stylesheet" href="themes.css">
    <meta charset="UTF-8">
    <style>
      #offre:checked ~ #ld, #demande:checked ~ #lo{display:none}
    </style>
  </head>
  <body>
      <h1>Effectuer une transaction</h1>
<?php
  $connexion = new PDO('pgsql:host=tuxa.sme.utc;dbname=dbbdd0a040','bdd0a040','r4HeOFDb');
?>
    <form method="get" action="transaction_post.php" oninput="balance.value=(parseFloat(Array.from(document.getElementById('mail').children).find(e => e.value == Mail_Utilisateur.value).attributes.solde.value)-parseFloat(Liste_Demande[Liste_Demande.selectedIndex].attributes.prix.value)).toFixed(2);document.getElementById('submit').disabled = (balance.value < 0 && document.getElementById('demande').checked)">
      Offre: <input id="offre" type="radio" name="Type" value="offre" checked>
      Demande: <input id="demande" type="radio" name="Type" value="demande"><br>
      Mail:
<?php
  $sql = 'SELECT Mail, Solde FROM UTILISATEUR ORDER BY MAIL;';
  $result = $connexion->prepare($sql);
  $result->execute();
  if ($result) { ?>
      <input type="email" list="mail" name="Mail_Utilisateur" required>
      <datalist id="mail">
<?php
    while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
?>
        <option value="<?php echo $row['mail']; ?>" solde="<?php echo $row['solde']; ?>">
<?php
    }
?>
      </datalist><br>
<?php
  } else {
?>
      <input type="email" size="50" name="Mail_Utilisateur" required><br>
<?php
  }
?>
      <div id="lo">List_Offre: 
<?php
  $sql = 'SELECT A.num_annonce, A.titre, A.prix FROM Annonce A JOIN Utilisateur U ON A.mail_utilisateur = U.mail WHERE type = \'offre\' AND U.solde >= A.prix AND NOT A.num_annonce IN (SELECT T.num_annonce FROM Transaction T) ORDER BY A.titre;';
  $result = $connexion->prepare($sql);
  $result->execute();
  if ($result) { ?>
      <select name="Liste_Offre">
<?php
    while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
?>
        <option value="<?php echo $row['num_annonce']; ?>"><?php echo $row['titre'].' - €'.$row['prix'].(substr($row['prix'], -2, -1) == '.' ? '0' : ''); ?></option>
<?php
    }
?>
      </select><br><br></div>
<?php
  } else {
?>
      <input type="number" min="1" name="Liste_Offre"><br><br></div>
<?php
  }
?>
      <div id="ld">List_Demande: 
<?php
  $sql = 'SELECT A.num_annonce, A.titre, A.prix FROM Annonce A JOIN Utilisateur U ON A.mail_utilisateur = U.mail WHERE type = \'demande\' AND NOT A.num_annonce IN (SELECT T.num_annonce FROM Transaction T) ORDER BY A.titre;';
  $result = $connexion->prepare($sql);
  $result->execute();
  if ($result) { ?>
      <select name="Liste_Demande">
<?php
    while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
?>
        <option value="<?php echo $row['num_annonce']; ?>" prix="<?php echo $row['prix']; ?>"><?php echo $row['titre'].' - €'.$row['prix'].(substr($row['prix'], -2, -1) == '.' ? '0' : ''); ?></option>
<?php
    }
?>
      </select><br>
      Bilan: €<output name="balance" for="Mail_Utilisateur Liste_Demande"></output></div><br>
<?php
  } else {
?>
      <input type="number" min="1" name="Liste_Demande"></div><br>
<?php
  }
?>
      <input id="submit" type="submit" />
    </form>
  </body>
</html>