<html>
  <head>
     <link rel="stylesheet" href="themes.css">
    <meta charset="UTF-8">
  </head>
  <body>
      <h1>Publication Annnonce</h1>
<?php
  $connexion = new PDO('pgsql:host=tuxa.sme.utc;dbname=dbbdd0a040','bdd0a040','r4HeOFDb');
?>
    <form method="get" action="publier_annonce_post.php">
      Titre: <input type="text" size="50" name="Titre" required/><br>
      Offre: <input type="radio" name="Type" value="offre" checked>
      Demande: <input type="radio" name="Type" value="demande"><br>
      Lieu: <input type="text" size="50" name="Lieu" required/><br>
      Description: <input type="text" size="70" name="Description"/><br>
      Prix: <input type="number" min="0" step="0.01" name="Prix" required/><br>
      Mail:
<?php
  $sql = 'SELECT Mail FROM UTILISATEUR ORDER BY MAIL;';
  $result = $connexion->prepare($sql);
  $result->execute();
  if ($result) { ?>
      <input type="email" list="mail" name="Mail_Utilisateur" required>
      <datalist id="mail">
<?php
    while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
?>
        <option value="<?php echo $row['mail']; ?>">
<?php
    }
?>
      </datalist><br>
<?php
  } else {
?>
      <input type="email" size="50" name="Mail_Utilisateur" required><br>
<?php
  }
?>
      Code_Produit: 
<?php
  $sql = 'SELECT Code, Nom FROM PRODUIT ORDER BY Nom;';
  $result = $connexion->prepare($sql);
  $result->execute();
  if ($result) { ?>
      <select name="Code_Produit" required>
<?php
    while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
?>
        <option value="<?php echo $row['code']; ?>"><?php echo $row['nom']; ?></option>
<?php
    }
?>
      </select><br>
<?php
  } else {
?>
      <input type="number" min="1" name="Code_Produit" required><br>
<?php
  }
?>
      <input type="submit" />
    </form>
  </body>
</html>