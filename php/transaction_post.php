<html>
<link rel="stylesheet" href="themes.css">
<head><meta charset="UTF-8"></head>
<body>
    <h1>Transaction</h1>
<p>
<?php

  $connexion = new PDO('pgsql:host=tuxa.sme.utc;dbname=dbbdd0a040','bdd0a040','r4HeOFDb');
  $connexion->setAttribute(PDO::ATTR_EMULATE_PREPARES, 1); //allows concurrent transactions

  // Could have the primary key auto-incriment using SQL instead
  $sql = 'SELECT Mail_Utilisateur FROM Annonce WHERE Num_Annonce = :num_annonce;';
  $result1 = $connexion->prepare($sql);

  $result1->bindValue(':num_annonce', ($_GET["Type"] == "offre" ? $_GET["Liste_Offre"] : $_GET["Liste_Demande"]), PDO::PARAM_STR);

  $result1->execute();

  if ($result1) {
    $row = $result1->fetch(PDO::FETCH_ASSOC);
    $mail_A = $row['mail_utilisateur'];

    $sql = 'SELECT Prix FROM ANNONCE WHERE Num_Annonce = :num_annonce;';
    $result2 = $connexion->prepare($sql);

    $result2->bindValue(':num_annonce', ($_GET["Type"] == "offre" ? $_GET["Liste_Offre"] : $_GET["Liste_Demande"]), PDO::PARAM_STR);

    $result2->execute();

    if ($result2) {
      $row = $result2->fetch(PDO::FETCH_ASSOC);
      $prix = $row['prix'];

      $sql = 'INSERT INTO TRANSACTION VALUES ((SELECT MAX(Num_Transaction) + 1 FROM TRANSACTION), :date_transaction, :prix, :methode, :mail_utilisateur, :mail_beneficiaire, :num_annonce); UPDATE Utilisateur SET Solde = Solde + :prix WHERE Mail = :mail_utilisateur; UPDATE Utilisateur SET Solde = Solde - :prix WHERE Mail = :mail_beneficiaire;';
      $result3 = $connexion->prepare($sql);
      
      date_default_timezone_set('Europe/Paris');
      $result3->bindValue(':date_transaction', date("Y-m-d"), PDO::PARAM_STR);
      $result3->bindValue(':prix', $prix, PDO::PARAM_STR);
      $result3->bindValue(':methode', 'Solde', PDO::PARAM_STR);

      //Keeping the mail_u and mail_b emails consistent with what was approved in BDR2.sql
      $result3->bindValue(':mail_utilisateur', ($_GET["Type"] == "offre" ? $_GET["Mail_Utilisateur"] : $mail_A), PDO::PARAM_STR);
      $result3->bindValue(':mail_beneficiaire', ($_GET["Type"] == "offre" ? $mail_A : $_GET["Mail_Utilisateur"]), PDO::PARAM_STR);

      //Is a ternary operator due to how we wrote the previous page
      $result3->bindValue(':num_annonce', ($_GET["Type"] == "offre" ? $_GET["Liste_Offre"] : $_GET["Liste_Demande"]), PDO::PARAM_INT);

      $result3->execute();

      if ($result3) {
        echo 'Nouveau inséré';
      }
      else {
        echo 'Erreur lors de l\'insertion';
      }
    }
    else {
      echo 'Erreur lors de l\'insertion';
    }
  }
  else {
    echo 'Erreur lors de l\'insertion';
  }

?>
</p>
</body>
</html>