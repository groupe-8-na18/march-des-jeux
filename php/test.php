<?php
  $connexion = new PDO('pgsql:host=tuxa.sme.utc;dbname=dbbdd0a040','bdd0a040','r4HeOFDb');

  // Could have the primary key auto-incriment using SQL instead
  $sql = 'INSERT INTO ANNONCE VALUES ((SELECT MAX(Num_Annonce) + 1 FROM ANNONCE), :titre, :type, :lieu, :date_annonce, :description, :prix, :mail_utilisateur, :code_produit);';
  $result = $connexion->prepare($sql);

  $result->bindValue(':titre', 'Testing', PDO::PARAM_STR);
  $result->bindValue(':type', 'offre', PDO::PARAM_STR);
  $result->bindValue(':lieu', 'fsd', PDO::PARAM_STR);
  $result->bindValue(':date_annonce', date("Y-m-d"), PDO::PARAM_STR);
  $result->bindValue(':description', 'GSFD', PDO::PARAM_STR);
  $result->bindValue(':prix', '2.99', PDO::PARAM_STR);
  $result->bindValue(':mail_utilisateur', 'vancouver.canucks@nhl.com', PDO::PARAM_STR);
  $result->bindValue(':code_produit', '5', PDO::PARAM_INT);

  $result->execute();

  if ($result) {
    echo 'Nouveau inséré';
  }
  else {
    echo 'Erreur lors de l\'insertion';
  }

?>
