**Nouvelle note de clarification: **


**I)**

**Objet > Utilisateur**

* email (chaîne) {unique}
* Nom d’utilisateur (chaîne)
* Nom (chaîne) 
* Prénom (chaîne) 
* Numéro téléphone (entier)
* Date de naissance (date)



**Objet> Utilisateur > Administrateur**

* niveau: [1..3] 

**Objet > Annonce**

* Numero de l’annonce (entier) {unique}
* Description : (chaîne)
* Lieu: (chaîne)
* Type {offre, demande}
* Titre: string
* Prix: float


Objet > Annonce > Offre	


**Objet> Annonce > Demande

Objet > Transaction **


* Date: (date)
* Numéro de transaction  (entier)
* Méthode de paiement { CB, Paypal, Solde }
* Montant : entier

  
**Objet > Produit**

* Nom (chaîne)
* Code
* Description
* Type {bien, service}


**Objet > Adresse **

* Nom de rue
* Numéro rue
* Code Postale
* Ville
* Pays




**Objet > Messagerie**


* Date: (date)
* Contenu: (string)


**Objet> Compte Argent**

* Numéro de compte (entier) {unique}
* Devise: chaîne
* Solde (float)

**Objet > Evaluation**

* Note:
* Description:

	
**II)**

**Fonctionnalités**

* Un administrateur modère les utilisateurs et leurs publications 
* Les utilisateurs postent et visualisent des annonces (de biens et services)
* Les utilisateurs pourront communiquer via une messagerie
* Le prix est fixe
* Un don est une offre avec un prix nul
* On considérera que même pour les demandes le prix sera renseigné ( demande de service gratuit alors prix nul ou service rémunéré alors un prix non nul par exemple)
* Les annonces sont de type Offre ou Demande de “produit” qui eux sont de type “services” ou bine “bien”.
* L’utilisateur à un nom d’utilisateur pour être identifié sur la plateforme comme sur la plupart des sites web
