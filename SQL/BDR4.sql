CREATE VIEW Vue_Transtraction AS
SELECT u.Num_Utilisateur, u.Nom, u.Prenom,
	t.Num_Transaction, t.Date_Transaction,
	a.Num_Annonce, a.Titre
FROM UTILISATEUR u, TRANSACTION t, ANNONCE a
WHERE u.Mail = t.Mail_Utilisateur AND t.Num_Annonce = a.Num_Annonce;


CREATE VIEW MessageEnvoyé AS
SELECT m.Date_Mess, m.Contenue, m.Mail_envoyeur
FROM MESSAGERIE m
GROUP BY m.Date_Mess, m.Contenue, m.Mail_envoyeur;


CREATE VIEW MessageReçu AS
SELECT m.Date_Mess, m.Contenue, m.Mail_receveur
FROM MESSAGERIE m
GROUP BY m.Date_Mess, m.Contenue, m.Mail_receveur;

CREATE VIEW Liste_Moderateur AS /* Liste les administrateurs*/
SELECT a.Mail_Admin
FROM ADMINISTRATEUR a;




CREATE VIEW PRODUITbien AS   /*Recherche  tout les biens*/
SELECT *
FROM PRODUIT
WHERE Type = 'bien';

CREATE VIEW PRODUITservice AS  /*Recherche  tout les services*/
SELECT *
FROM PRODUIT
WHERE Type = 'service';

CREATE VIEW ANNONCEOffre AS  /*Recherche  toutes les offres */
SELECT *
FROM ANNONCE
WHERE Type='offre';

CREATE VIEW ANNONCEdemande AS  /*Recherche  toutes les demandes */
SELECT *
FROM ANNONCE
WHERE Type='demande';

CREATE VIEW TransCB AS    /*Recherche  toutes les transactions par carte bancaire*/
SELECT *
FROM TRANSACTION
WHERE Methode='CB';

CREATE VIEW TransSolde AS  /*Recherche  toutes les transactions payées par solde */
SELECT *
FROM TRANSACTION
WHERE Methode='Solde';

CREATE VIEW TransPaypal AS /*Recherche  toutes les transactions par paypal*/
SELECT *
FROM TRANSACTION
WHERE Methode='Paypal';

SELECT Mail_Noté  /* renvoie le mail des personnes ayants eu au moins 5 notes supérieur à 4 lors d'une transaction*/
FROM EVALUATION
WHERE Note >=4
GROUP BY (Mail_Noté)
HAVING COUNT(*) >=5;
