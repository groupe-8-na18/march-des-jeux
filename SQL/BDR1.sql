DROP VIEW IF EXISTS TransPaypal;
DROP VIEW IF EXISTS TransSolde;
DROP VIEW IF EXISTS TransCB;
DROP VIEW IF EXISTS ANNONCEdemande;
DROP VIEW IF EXISTS ANNONCEOffre;
DROP VIEW IF EXISTS PRODUITservice;
DROP VIEW IF EXISTS PRODUITbien;
DROP VIEW IF EXISTS Liste_Moderateur;
DROP VIEW IF EXISTS MessageReçu;
DROP VIEW IF EXISTS MessageEnvoyé;
DROP VIEW IF EXISTS Vue_Transtraction;

DROP TABLE IF EXISTS MESSAGERIE;
DROP TABLE IF EXISTS EVALUATION;
DROP TABLE IF EXISTS PARCOURS;
DROP TABLE IF EXISTS TRANSACTION;
DROP TABLE IF EXISTS MODERE;
DROP TABLE IF EXISTS ANNONCE;
DROP TABLE IF EXISTS ADMINISTRATEUR;
DROP TABLE IF EXISTS UTILISATEUR;
DROP TABLE IF EXISTS PRODUIT;




CREATE TABLE PRODUIT(
  Code integer NOT NULL,
  Nom varchar (50) NOT NULL,
  Description varchar (500),
  Type varchar(7),
  PRIMARY KEY (Code),
  CHECK (Type in('bien','service'))
  
  );

CREATE TABLE UTILISATEUR (
  Num_Utilisateur char(8) UNIQUE NOT NULL,
  Mail varchar (50) NOT NULL,
  Nom varchar (15) NOT NULL,
  Prenom varchar (50) NOT NULL,
  Num_tel varchar(12) NOT NULL,
  Date_Naissance date NOT NULL,
  Nom_adresse varchar(50) NOT NULL,
  CP varchar(7) NOT NULL,
  Ville varchar(50) NOT NULL,
  Pays varchar (50) NOT NULL,
  Num_Compte integer UNIQUE NOT NULL,
  Solde float NOT NULL,
  PRIMARY KEY (Mail)
  
);

CREATE TABLE ADMINISTRATEUR (
  niveau integer,
  Mail_Admin varchar (50) NOT NULL,
  FOREIGN KEY (Mail_Admin) REFERENCES UTILISATEUR (Mail),
  CHECK (niveau BETWEEN 1 AND 3)
);

CREATE TABLE ANNONCE (
  Num_Annonce integer NOT NULL,
  Titre varchar (50) NOT NULL,
  Type char(7),
  Lieu varchar (50) NOT NULL,
  Date_annonce date NOT NULL,
  Description varchar (70),
  Prix float,
  Mail_Utilisateur varchar (50) NOT NULL,
  Code_Produit integer NOT NULL,
  PRIMARY KEY (Num_Annonce),
  FOREIGN KEY (Mail_Utilisateur) REFERENCES UTILISATEUR (Mail),
  FOREIGN KEY (Code_Produit) REFERENCES PRODUIT (Code),
  CHECK (Type in ('offre','demande')),
  CHECK (Prix >= 0)
  
  );
CREATE TABLE MODERE (
  Mail_Admin varchar (50) NOT NULL,
  Mail_Modéré varchar(50) NOT NULL,
  PRIMARY KEY (Mail_Admin,Mail_Modéré),
  FOREIGN KEY (Mail_Admin) REFERENCES UTILISATEUR ( Mail),
  FOREIGN KEY (Mail_Modéré) REFERENCES UTILISATEUR (Mail),
  CHECK (Mail_Admin <> Mail_Modéré)
  );

CREATE TABLE TRANSACTION(
  Num_Transaction integer NOT NULL,
  Date_Transaction date NOT NULL,
  Montant float,
  Methode varchar(6),
  Mail_Utilisateur varchar(50) NOT NULL,
  Mail_beneficiaire varchar (50) NOT NULL,
  Num_Annonce integer UNIQUE NOT NULL,
  PRIMARY KEY (Num_Transaction),
  FOREIGN KEY (Mail_Utilisateur) REFERENCES UTILISATEUR (Mail),
  FOREIGN KEY (Mail_beneficiaire) REFERENCES UTILISATEUR (Mail),
  FOREIGN KEY (Num_Annonce) REFERENCES ANNONCE (Num_Annonce),
  CHECK (Methode in('CB','Paypal','Solde'))
  
  );
CREATE TABLE PARCOURS (
  Mail_Utilisateur varchar(50) NOT NULL,
  Num_Annonce integer NOT NULL,
  PRIMARY KEY (Mail_Utilisateur,Num_Annonce),
  FOREIGN KEY (Mail_Utilisateur) REFERENCES UTILISATEUR (Mail),
  FOREIGN KEY (Num_Annonce) REFERENCES ANNONCE (Num_Annonce)
);

CREATE TABLE EVALUATION(
  Note integer,
  Description varchar (100),
  Mail_noteur varchar(50) NOT NULL,
  Mail_noté varchar(50) NOT NULL,
  Num_Annonce integer NOT NULL,
  PRIMARY KEY (Mail_noté,Mail_noteur,Num_Annonce),
  FOREIGN KEY (Mail_noté) REFERENCES UTILISATEUR(Mail),
  FOREIGN KEY (Mail_noteur) REFERENCES UTILISATEUR (Mail),
  FOREIGN KEY (Num_Annonce) REFERENCES ANNONCE(Num_Annonce),
  CHECK (Note BETWEEN 0 AND 5),
  CHECK(Mail_noté<>Mail_Noteur)
);

CREATE TABLE MESSAGERIE(
  Date_mess TIMESTAMP NOT NULL,
  Contenue varchar(80),
  Mail_envoyeur varchar(50) NOT NULL,
  Mail_receveur varchar(50) NOT NULL,
  PRIMARY KEY(Mail_envoyeur,Mail_receveur,Date_mess),
  FOREIGN KEY (Mail_envoyeur) REFERENCES UTILISATEUR (Mail),
  FOREIGN KEY (Mail_receveur) REFERENCES UTILISATEUR (Mail),
  CHECK (Mail_envoyeur<>Mail_receveur)
);
