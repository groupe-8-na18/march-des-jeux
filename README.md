Arthur Jouglet – Thomas Dedinsky - Adrien Charannat - Groupe 8  
« Marché au Jeu »

( Absent : CATERIANO LINARES Claudia)

L'objectif du projet est de réaliser une place d'échange à l'UTC.

Le projet pourra intégrer des biens et des services.

Le projet pourra intégrer des actions de dons, d'échange ou de vente.

Le projet pourra intégrer la gestion d'une monnaie interne.

Les utilisateurs pourront déposer des offres ou des demandes, qui pourront être localisées dans le temps et l'espace.


Le projet pourra intégrer un système de communication interne.

Le projet pourra intégrer un historique des actions menées.



Dans un premier temps, nous devons donc réaliser la Note de Clarification qui nous permettra d’avancer dans le design de la BDD.

Les fichiers V2 sont les nouvelles versions à prendre en compte, qui se base sur le cdc et sont donc actualisés selon le bon contexte.

Si vous le suhaitez, vous pouvez tester les commandes à l'aide des identifiants suivants :
-login: anaheim.ducks@nhl.com
-password: password